package middleware

import (
	"log"
	"strconv"
	"strings"

	"learn-raa/profile-service/config"
	"learn-raa/profile-service/exception"
	"learn-raa/profile-service/helper"
	"learn-raa/profile-service/model/web"

	"github.com/gofiber/fiber/v2"
)

func IsAuthenticated(c *fiber.Ctx) error {
	cookie := c.Cookies("token")

	if _, _, _, err := helper.ParseJwt(cookie); err != nil {
		if strings.Contains(err.Error(), "token is expired") {
			return c.Status(401).JSON(web.WebResponse{
				Code:    99281,
				Status:  false,
				Message: "token expired",
			})
		}
		return c.Status(fiber.StatusUnauthorized).JSON(web.WebResponse{
			Code:    fiber.StatusUnauthorized,
			Status:  false,
			Message: "unauthorized",
		})
	}

	return c.Next()
}

func IsAdmin(c *fiber.Ctx) bool {
	cookie := c.Cookies("token")

	if _, level, _, err := helper.ParseJwt(cookie); err != nil || level != config.VariableRoleAdmin {
		return false
	}

	return true
}

func AddUserAuthorize() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		cookie := ctx.Cookies("token")

		_, _, permissions, err := helper.ParseJwt(cookie)
		permissionCreate, _ := strconv.Atoi(config.PermissionAddUser)

		if err != nil {
			log.Println(err)
			return exception.ErrorHandler(ctx, fiber.NewError(401, "Unauthorize."))
		}

		grant := false

		//if level == config.VariableRoleAdmin {
		//	grant = true
		//} else {
		for _, permission := range permissions {
			if permission.PermissionId.(float64) == float64(permissionCreate) {
				grant = true
			}
		}
		//}

		if grant {
			return ctx.Next()
		} else {
			return exception.ErrorHandler(ctx, fiber.NewError(fiber.StatusForbidden, "Unauthorize."))
		}
	}
}

func ImportUserAuthorize() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		cookie := ctx.Cookies("token")

		_, _, permissions, err := helper.ParseJwt(cookie)
		permissionImport, _ := strconv.Atoi(config.PermissionImportUser)

		if err != nil {
			log.Println(err)
			return exception.ErrorHandler(ctx, fiber.NewError(401, "Unauthorize."))
		}

		grant := false

		//if level == config.VariableRoleAdmin {
		//	grant = true
		//} else {
		for _, permission := range permissions {
			if permission.PermissionId.(float64) == float64(permissionImport) {
				grant = true
			}
		}
		//}

		if grant {
			return ctx.Next()
		} else {
			return exception.ErrorHandler(ctx, fiber.NewError(fiber.StatusForbidden, "Unauthorize."))
		}
	}
}

func ManageUserAuthorize() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		cookie := ctx.Cookies("token")
		nik := ctx.Params("nik")

		actor, _, permissions, err := helper.ParseJwt(cookie)
		permissionCreate, _ := strconv.Atoi(config.PermissionManageUser)

		if err != nil {
			log.Println(err)
			return exception.ErrorHandler(ctx, fiber.NewError(401, "Unauthorize."))
		}

		grant := false

		//if level == config.VariableRoleAdmin {
		//	grant = true
		//} else
		if nik == actor {
			grant = true
		} else {
			for _, permission := range permissions {
				if permission.PermissionId.(float64) == float64(permissionCreate) {
					grant = true
				}
			}
		}

		if grant {
			return ctx.Next()
		} else {
			return exception.ErrorHandler(ctx, fiber.NewError(fiber.StatusForbidden, "Unauthorize."))
		}
	}
}

func ActivateUserAuthorize(ctx *fiber.Ctx) bool {

	cookie := ctx.Cookies("token")

	_, _, permissions, err := helper.ParseJwt(cookie)
	permissionActivate, _ := strconv.Atoi(config.PermissionActivateUser)

	if err != nil {
		return false
	}

	grant := false

	//if level == config.VariableRoleAdmin {
	//	grant = true
	//} else {
	for _, permission := range permissions {
		if permission.PermissionId.(float64) == float64(permissionActivate) {
			log.Println(permission.PermissionId.(float64), float64(permissionActivate))
			grant = true
		}
	}
	//}

	return grant
}

func DeactiveUserAuthorize(ctx *fiber.Ctx) bool {

	cookie := ctx.Cookies("token")

	_, _, permissions, err := helper.ParseJwt(cookie)
	permissionDeactive, _ := strconv.Atoi(config.PermissionDeactiveUser)

	if err != nil {
		return false
	}

	grant := false

	//if level == config.VariableRoleAdmin {
	//	grant = true
	//} else {
	for _, permission := range permissions {
		if permission.PermissionId.(float64) == float64(permissionDeactive) {
			log.Println(permission.PermissionId.(float64), float64(permissionDeactive))
			grant = true
		}
	}
	//}

	return grant
}
