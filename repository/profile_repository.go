package repository

import (
	"context"

	"learn-raa/profile-service/model/domain"
)

type ProfileRepository interface {
	FindProfileNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.User, error)
	FindProfileByQueryTx(ctx context.Context, query, value string) (domain.User, error)
	UpdatePasswordTx(ctx context.Context, user domain.User) error
	UpdateProfileTx(ctx context.Context, user domain.User) error
}
