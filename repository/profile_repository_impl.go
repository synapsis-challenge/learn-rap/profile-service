package repository

import (
	"context"

	"learn-raa/profile-service/model/domain"

	"github.com/jackc/pgx/v5"
)

type ProfileRepositoryImpl struct {
	DB Store
}

func NewProfileRepository(db Store) ProfileRepository {
	return &ProfileRepositoryImpl{
		DB: db,
	}
}

func (repository *ProfileRepositoryImpl) FindProfileNotDeleteByQueryTx(ctx context.Context, query, value string) (domain.User, error) {

	var data domain.User
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindProfileNotDeletedByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *ProfileRepositoryImpl) FindProfileByQueryTx(ctx context.Context, query, value string) (domain.User, error) {

	var data domain.User
	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		data, err = repository.FindProfileByQuery(ctx, tx, query, value)
		if err != nil {
			return err
		}

		return nil
	})

	return data, err
}

func (repository *ProfileRepositoryImpl) UpdatePasswordTx(ctx context.Context, profile domain.User) error {

	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err = repository.UpdatePasswordProfile(ctx, tx, profile)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (repository *ProfileRepositoryImpl) UpdateProfileTx(ctx context.Context, profile domain.User) error {

	var err error

	err = repository.DB.WithTransaction(ctx, func(tx pgx.Tx) error {

		err = repository.UpdateProfile(ctx, tx, profile)
		if err != nil {
			return err
		}

		return nil
	})

	return err
}
