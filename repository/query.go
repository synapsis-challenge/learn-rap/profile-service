package repository

import (
	"context"
	"fmt"
	"log"

	"learn-raa/profile-service/model/domain"

	"github.com/jackc/pgx/v5"
)

func (repository *ProfileRepositoryImpl) FindProfileByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.User, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	
	WHERE %s = $1`, "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil
}

func (repository *ProfileRepositoryImpl) FindProfileNotDeletedByQuery(ctx context.Context, db pgx.Tx, query, value string) (domain.User, error) {
	queryStr := fmt.Sprintf(`SELECT u.* FROM %s u
	
	WHERE %s = $1 AND deleted_at is NULL`, "users", query)

	user, err := db.Query(context.Background(), queryStr, value)

	if err != nil {
		return domain.User{}, err
	}

	defer user.Close()

	data, err := pgx.CollectOneRow(user, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil
}

func (repository *ProfileRepositoryImpl) UpdateProfile(ctx context.Context, db pgx.Tx, user domain.User) error {
	query := fmt.Sprintf("UPDATE %s SET name = $1, phone = $2, updated_at = $3, nomor_wa = $4 WHERE nik = $5 AND deleted_at is NULL", "users")
	_, err := db.Prepare(context.Background(), "update_profile", query)
	if err != nil {
		return err
	}

	_, err = db.Exec(context.Background(), "update_profile",
		user.Name, user.Phone, user.UpdatedAt, user.NomorWa, user.NIK)

	if err != nil {
		log.Println("exec update", err)
		return err
	}

	return nil
}

func (repository *ProfileRepositoryImpl) UpdatePasswordProfile(ctx context.Context, db pgx.Tx, user domain.User) error {
	query := "UPDATE users SET password = $1 WHERE nik = $2"
	_, err := db.Prepare(context.Background(), "update_pass_profile", query)
	if err != nil {
		return err
	}

	data, err := db.Query(context.Background(), "update_pass_profile", user.Password, user.NIK)
	if err != nil {
		return err
	}

	defer data.Close()
	return nil
}
