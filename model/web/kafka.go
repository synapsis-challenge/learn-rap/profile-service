package web

type KafkaImportUserMessage struct {
	ActionBy  string
	ProjectID int
	Url       string
}
