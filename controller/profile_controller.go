package controller

import (
	"fmt"
	"time"

	"learn-raa/profile-service/config"
	"learn-raa/profile-service/exception"
	"learn-raa/profile-service/helper"
	"learn-raa/profile-service/middleware"
	"learn-raa/profile-service/model/web"
	"learn-raa/profile-service/service"

	"github.com/gofiber/fiber/v2"
)

type ProfileControllerImpl struct {
	ProfileService service.ProfileService
}

type ProfileController interface {
	NewProfileRouter(app *fiber.App)
}

func NewProfileController(profileService service.ProfileService) ProfileController {
	return &ProfileControllerImpl{
		ProfileService: profileService,
	}
}

func (controller *ProfileControllerImpl) NewProfileRouter(app *fiber.App) {
	user := app.Group(config.EndpointPrefix)
	user.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	user.Use(middleware.IsAuthenticated)
	user.Get("/", controller.GetProfileById)
	user.Put("/", controller.UpdateUser)
	user.Put("/password", controller.UpdatePasswordProfile)
}

func (controller *ProfileControllerImpl) GetProfileById(ctx *fiber.Ctx) error {
	actor, _, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	response, err := controller.ProfileService.FindProfileNotDeleteByNik(ctx, actor)

	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *ProfileControllerImpl) UpdateUser(ctx *fiber.Ctx) error {
	var request web.UserUpdateRequest
	err := ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	actor, _, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	request.NIK = actor

	userResponse, err := controller.ProfileService.UpdateProfile(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	action := fmt.Sprintf("updated user %s", userResponse.Name)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: userResponse.Name,
		Category:  config.CategoryService,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *ProfileControllerImpl) UpdatePasswordProfile(ctx *fiber.Ctx) error {
	var request web.UserUpdatePasswordRequest
	err := ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	actor, _, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	request.NIK = actor

	userResponse, err := controller.ProfileService.UpdatePasswordProfile(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	action := fmt.Sprintf("updated password user %s", userResponse.Name)
	data := web.LogCreateRequest{
		Actor:     actor,
		ActorName: userResponse.Name,
		// Project:   userResponse.ProjectName,
		Category:  config.CategoryService,
		Action:    action,
		Timestamp: time.Now(),
	}
	helper.ProduceToKafka(data, "POST.LOG", config.KafkaLogTopic)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}
