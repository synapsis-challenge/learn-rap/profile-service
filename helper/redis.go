package helper

import (
	"encoding/json"
	"learn-raa/profile-service/config"
	"learn-raa/profile-service/model/domain"
	"log"

	"github.com/go-redis/redis"
)

func RedisConnection() (*redis.Client, error) {
	log.Printf("REDIS | Trying to connect to server...")

	client := redis.NewClient(&redis.Options{
		Addr:     config.RedisHost + ":" + config.RedisPort,
		Password: "",
		DB:       0,
	})

	pong, err := client.Ping().Result()
	log.Printf("REDIS | PONG Response: %s %s\n", pong, err)

	return client, err
}

func RedisGet(key string) (string, error) {
	client, err := RedisConnection()
	if err != nil {
		log.Println(err)
	}

	val, err := client.Get(key).Result()
	if err != nil {
		log.Println(err)
	}

	return val, err
}

func RedisSet(key string, data domain.User) error {
	client, err := RedisConnection()
	if err != nil {
		log.Println(err)
	}

	jsonUser, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
	}

	err = client.Set(key, jsonUser, 0).Err()
	if err != nil {
		log.Println(err)
	}

	return err
}

func RedisDel(key string) error {
	client, err := RedisConnection()
	if err != nil {
		log.Println(err)
	}

	_, err = client.Del(key).Result()
	if err != nil {
		log.Println(err)
	}

	return err
}
