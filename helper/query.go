package helper

import (
	"fmt"
	"strconv"

	"learn-raa/profile-service/config"
)

func QueryPage(query map[string]string) string {
	var queryStr string
	if query["page"] != "" {
		pageInt, _ := strconv.Atoi(query["page"])
		if query["limit"] != "" {
			limitInt, _ := strconv.Atoi(query["limit"])
			queryStr = fmt.Sprintf(" OFFSET %d LIMIT %d", (pageInt-1)*limitInt, limitInt)
		} else {
			defaultLimit, _ := strconv.Atoi(config.DefaultLimit)
			queryStr = fmt.Sprintf(" OFFSET  %d LIMIT %d", (pageInt-1)*defaultLimit, defaultLimit)
		}
	} else {
		if query["limit"] != "" {
			limitInt, _ := strconv.Atoi(query["limit"])
			queryStr += fmt.Sprintf("LIMIT %d", limitInt)
		}
	}

	return queryStr
}
