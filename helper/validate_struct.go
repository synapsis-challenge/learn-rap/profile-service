package helper

import (
	"regexp"
	"strings"

	"learn-raa/profile-service/exception"
)

func IsEmailValid(e string) bool {
	emailRegex := regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`)
	return emailRegex.MatchString(e)
}

func IsPhoneValid(e string) bool {
	phoneRegex := regexp.MustCompile("^('+62|62|0)8[1-9][0-9][0-9]{6,9}$")
	return phoneRegex.MatchString(e)
}

func ValidateStruct(err error) error {
	if strings.Contains(err.Error(), "NIK") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("NIK required.")
	}
	if strings.Contains(err.Error(), "RoleId") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Role Id required.")
	}
	if strings.Contains(err.Error(), "DepartementId") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Departement Id required.")
	}
	if strings.Contains(err.Error(), "ProjectId") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Project Id required.")
	}
	if strings.Contains(err.Error(), "Gender") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Gender Id required.")
	}
	if strings.Contains(err.Error(), "Religion") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Religion Id required.")
	}
	if strings.Contains(err.Error(), "Name") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Name required.")
	}
	if strings.Contains(err.Error(), "Email") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Email required.")
	}
	if strings.Contains(err.Error(), "Password") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Password required.")
	}
	if strings.Contains(err.Error(), "Phone") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Phone required.")
	}
	if strings.Contains(err.Error(), "Status") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Status required.")
	}
	if strings.Contains(err.Error(), "Phone") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Phone required.")
	}
	if strings.Contains(err.Error(), "DateOfHire") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Date of hire required.")
	}
	return exception.ErrorBadRequest(err.Error())
}
