package service

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"learn-raa/profile-service/config"
	"learn-raa/profile-service/model/domain"

	"github.com/gofiber/fiber/v2"
	"github.com/jackc/pgx/v5"
)

func InputUser(users []byte) error {
	var user domain.User

	if err := json.Unmarshal(users, &user); err != nil {
		log.Println(err)
	}

	db := config.NewPostgresPool()

	tx, err := db.BeginTx(context.TODO(), pgx.TxOptions{})

	if err != nil {
		log.Panic(err)
	}

	query := fmt.Sprintf(`INSERT INTO %s (nik, name, email, password, phone, status, created_at, updated_at, nomor_wa)
	VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9)`, "users")
	_, err = tx.Prepare(context.Background(), "input_user", query)
	if err != nil {
		return fiber.NewError(500, err.Error())
	}

	_, err = tx.Exec(context.Background(), "input_user",
		user.NIK, user.Name, user.Email, user.Password, user.Phone, user.Status, user.CreatedAt, user.UpdatedAt, user.NomorWa)
	if err != nil {
		tx.Rollback(context.TODO())
		panic(err)
	}

	tx.Commit(context.TODO())

	return nil
}

func UpdateUser(users []byte) error {
	var user domain.User

	if err := json.Unmarshal(users, &user); err != nil {
		log.Println(err)
	}

	db := config.NewPostgresPool()

	tx, err := db.BeginTx(context.TODO(), pgx.TxOptions{})
	if err != nil {
		log.Panic(err)
	}

	query := fmt.Sprintf("UPDATE %s SET name = $1, email = $2, phone = $3, updated_at = $4, nomor_wa = $5 WHERE nik = $6 AND deleted_at is NULL", "users")
	_, err = tx.Prepare(context.Background(), "data", query)
	if err != nil {
		return fiber.NewError(500, err.Error())
	}

	_, err = tx.Exec(context.Background(), "data",
		user.Name, user.Email, user.Phone, user.UpdatedAt, user.NomorWa, user.NIK)

	if err != nil {
		tx.Rollback(context.TODO())
		panic(err)
	}

	tx.Commit(context.TODO())
	db.Close()

	return nil
}

func DeleteUser(users []byte) error {
	var user domain.User

	if err := json.Unmarshal(users, &user); err != nil {
		log.Println(err)
	}

	db := config.NewPostgresPool()

	tx, err := db.BeginTx(context.TODO(), pgx.TxOptions{})
	if err != nil {
		log.Panic(err)
	}

	query := "UPDATE users SET deleted_at = $1 WHERE nik = $2"
	_, err = tx.Prepare(context.Background(), "data", query)
	if err != nil {
		return fiber.NewError(500, err.Error())
	}

	_, err = tx.Exec(context.Background(), "data", user.DeletedAt, user.NIK)

	if err != nil {
		tx.Rollback(context.TODO())
		panic(err)
	}

	tx.Commit(context.TODO())
	db.Close()

	return nil
}
