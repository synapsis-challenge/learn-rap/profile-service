package service

import (
	"log"
	"strings"
	"time"

	"learn-raa/profile-service/config"
	"learn-raa/profile-service/exception"
	"learn-raa/profile-service/helper"
	"learn-raa/profile-service/model/domain"
	"learn-raa/profile-service/model/web"
	"learn-raa/profile-service/repository"

	"github.com/go-playground/validator"
	"github.com/gofiber/fiber/v2"
)

type ProfileServiceImpl struct {
	ProfileRepository repository.ProfileRepository
	Validate          *validator.Validate
}

type ProfileService interface {
	FindProfileNotDeleteByNik(ctx *fiber.Ctx, nik string) (web.UserResponse, error)
	UpdateProfile(ctx *fiber.Ctx, request web.UserUpdateRequest) (web.UserResponse, error)
	UpdatePasswordProfile(ctx *fiber.Ctx, request web.UserUpdatePasswordRequest) (web.UserResponse, error)
}

func NewProfileService(profileRepository repository.ProfileRepository, validate *validator.Validate) ProfileService {
	return &ProfileServiceImpl{
		ProfileRepository: profileRepository,
		Validate:          validate,
	}
}

func (service *ProfileServiceImpl) FindProfileNotDeleteByNik(ctx *fiber.Ctx, nik string) (web.UserResponse, error) {
	user, err := service.ProfileRepository.FindProfileNotDeleteByQueryTx(ctx.Context(), "nik", nik)

	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not found.")
	}

	if *user.Status == 0 {
		return web.UserResponse{}, exception.ErrorUnauthorize("User not active.")
	}

	return domain.ToUserResponse(user), nil
}

func (service *ProfileServiceImpl) UpdateProfile(ctx *fiber.Ctx, request web.UserUpdateRequest) (web.UserResponse, error) {
	err := service.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return web.UserResponse{}, err
	}

	if len(request.Name) > 50 {
		return web.UserResponse{}, exception.ErrorBadRequest("Name should less than equal 50.")
	}

	if len(request.Name) == 0 {
		return web.UserResponse{}, exception.ErrorBadRequest("Name can't be empty.")
	}

	if !helper.IsPhoneValid(request.Phone) {
		return web.UserResponse{}, exception.ErrorBadRequest("Not valid phone.")
	}

	if !helper.IsEmailValid(request.Email) {
		return web.UserResponse{}, exception.ErrorBadRequest("Not valid email.")
	}

	user, err := service.ProfileRepository.FindProfileNotDeleteByQueryTx(ctx.Context(), "nik", request.NIK)
	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not exist.")
	}

	userEmail, _ := service.ProfileRepository.FindProfileNotDeleteByQueryTx(ctx.Context(), "email", request.Email)
	if userEmail.NIK != "" && userEmail.NIK != request.NIK {
		return web.UserResponse{}, exception.ErrorBadRequest("Email exist.")
	}
	user.Email = request.Email

	userPhone, _ := service.ProfileRepository.FindProfileNotDeleteByQueryTx(ctx.Context(), "phone", request.Phone)
	if userPhone.NIK != "" && userPhone.NIK != request.NIK {
		return web.UserResponse{}, exception.ErrorBadRequest("Phone exist.")
	}
	user.Phone = request.Phone

	userWa, _ := service.ProfileRepository.FindProfileNotDeleteByQueryTx(ctx.Context(), "nomor_wa", request.NomorWa)
	if userWa.NIK != "" && userWa.NIK != request.NIK {
		return web.UserResponse{}, exception.ErrorBadRequest("Whatsapp number exist.")
	}
	user.NomorWa = request.NomorWa

	user.Name = request.Name
	user.UpdatedAt = time.Now()

	err = service.ProfileRepository.UpdateProfileTx(ctx.Context(), user)
	if err != nil {
		return web.UserResponse{}, err
	}
	// user.FingerId = base64.StdEncoding.EncodeToString([]byte(user.FingerId))

	helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopic)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicAuth)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicUser)

	// redis
	redisKey := "user:" + user.NIK

	// redis set
	err = helper.RedisSet(redisKey, user)
	if err != nil {
		log.Println(err)
	}

	// finger, _ := base64.StdEncoding.DecodeString(user.FingerId)
	// user.FingerId = string(finger)

	return domain.ToUserResponse(user), nil
}

func (service *ProfileServiceImpl) UpdatePasswordProfile(ctx *fiber.Ctx, request web.UserUpdatePasswordRequest) (web.UserResponse, error) {
	err := service.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return web.UserResponse{}, err
	}

	user, err := service.ProfileRepository.FindProfileNotDeleteByQueryTx(ctx.Context(), "nik", request.NIK)
	if err != nil || user.NIK == "" {
		return web.UserResponse{}, exception.ErrorNotFound("User not exist.")
	}

	err = user.ComparePassword(user.Password, request.OldPassword)
	if err != nil {
		return web.UserResponse{}, exception.ErrorBadRequest("Kata sandi lama anda salah.")
	}

	if strings.Compare(request.Password, request.ConfirmPass) != 0 {
		return web.UserResponse{}, exception.ErrorBadRequest("Password didn't match with password confirm.")
	}

	if len(request.Password) < 6 {
		return web.UserResponse{}, exception.ErrorBadRequest("Password should more then 6 character.")
	}

	user.SetPassword(request.Password)
	user.UpdatedAt = time.Now()

	err = service.ProfileRepository.UpdatePasswordTx(ctx.Context(), user)
	if err != nil {
		return web.UserResponse{}, err
	}
	// user.FingerId = base64.StdEncoding.EncodeToString([]byte(user.FingerId))

	helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopic)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicAuth)
	// helper.ProduceToKafka(user, "PUT.USER", config.KafkaTopicUser)

	// redis
	redisKey := "user:" + user.NIK

	// redis set
	err = helper.RedisSet(redisKey, user)
	if err != nil {
		log.Println(err)
	}

	// finger, _ := base64.StdEncoding.DecodeString(user.FingerId)
	// user.FingerId = string(finger)

	return domain.ToUserResponse(user), nil
}
